<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
        	'name' => 'Admin',
            'public' => 0,
        ]);

        DB::table('roles')->insert([
        	'name' => 'Customer',
            'public' => 1,
        ]);

        DB::table('roles')->insert([
            'name' => 'Tour Host',
            'public' => 1,
        ]);

    }
}
