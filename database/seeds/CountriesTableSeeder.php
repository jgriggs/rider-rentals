<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
        	'name' => 'United Kingdom',
            'short_name' => 'gb',
        ]);

        DB::table('countries')->insert([
        	'name' => 'France',
            'short_name' => 'fr',
        ]);

        DB::table('countries')->insert([
            'name' => 'Spain',
            'short_name' => 'es',
        ]);

    }
}
