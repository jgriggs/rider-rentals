<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompaniesAddStripeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function(Blueprint $table)
        {
            $table->string('stripe_id', 64)->nullable();
            $table->string('postcode')->nullable();
            $table->dropColumn('country');
            $table->integer('country_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function(Blueprint $table)
        {
            $table->dropColumn('stripe_id');
            $table->dropColumn('country_id');
            $table->dropColumn('postcode');
            $table->integer('country')->unsigned()->nullable();
        });
    }
}
