function toggleCompanyNumber() {
    companyType = $('select[name="type"]').val();
    if (companyType === 'ltd' || companyType === 'plc')
        $('.registration_number').removeClass('hidden');
    else
        $('.registration_number').addClass('hidden');
}

function toggleVat() {
    vatRegistered = $('input[type="radio"]:checked').val();
    if (vatRegistered === 'yes')
        $('.vat-number').removeClass('hidden');
    else
        $('.vat-number').addClass('hidden');
}

$(document).ready(function()
{
    toggleCompanyNumber();
    toggleVat();
});

// Event listeners
$('select[name="type"]').on('change', toggleCompanyNumber);
$('input[type="radio"]').on('change', toggleVat);