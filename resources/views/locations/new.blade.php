@extends('layouts.app')

@section('content')
	<div class="page col-md-6 col-md-offset-3">
		<div class="heading">
			<h1>New Location</h1>
		</div>
		<div class="content">
			Create a location for your company, this will allow you to sell your tours.
			<hr>

			@if(count($errors))
			<div class="errors">
				@foreach($errors->all() as $error)
					<span>{{ $error }}</span>
				@endforeach
			</div>

			<hr>
			@endif

			<a href="#" id="prefillLocation">Fill fields with company location?</a>
			<form action="{{ url('companies/locations/new') }}" method="post">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-md-12">
			    		<input {{ $errors->has('name') ? 'class=has-error' : '' }} type="text" name="name" placeholder="Location Name" value="{{ old('name') }}">
			    	</div>
					<div class="col-md-12">
			    		<input {{ $errors->has('address_line_one') ? 'class=has-error' : '' }} type="text" name="address_line_one" placeholder="Address Line One" value="{{ old('address_line_one') }}">
			    	</div>
					<div class="col-md-12">
			    		<input type="text" name="address_line_two" placeholder="Address Line Two" value="{{ old('address_line_two') }}">
			    	</div>
					<div class="col-md-12">
			    		<input {{ $errors->has('city') ? 'class=has-error' : '' }} type="text" name="city" placeholder="Town / City" value="{{ old('city') }}">
			    	</div>
					<div class="col-md-12">
			    		<input {{ $errors->has('postcode') ? 'class=has-error' : '' }} type="text" name="postcode" placeholder="Postcode" value="{{ old('postcode') }}">
			    	</div>
					<div class="col-md-12">
						<select {{ $errors->has('country_id') ? 'class=has-error' : '' }} name="country_id">
							<option value="">Please select your Country</option>
							@foreach(\RiderRentals\Country::orderBy('name')->get() as $country)
							<option value="{{ $country->id }}" {{ (old('country') == $country->id ? 'selected':'') }} >
								{{ $country->name }}
							</option>
							@endforeach
						</select>
			    	</div>
			    	<input type="hidden" name="company_id" value="{{ $companyId }}">
				</div>

				<hr>

				<div class="row">
					<div class="col-md-6 col-md-offset-6 text-right">
					    <button type="submit" class="button button-large button-primary">
					        Add Location
					    </button>
			    	</div>
				</div>
			</form>
		</div>
	</div>
@endsection