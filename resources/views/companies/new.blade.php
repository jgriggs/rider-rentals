@extends('layouts.app')

@section('content')

<h1>Register Your Company</h1>

@if(count($errors))
<div class="errors">
	@foreach($errors->all() as $error)
		<span>{{ $error }}</span>
	@endforeach
</div>
@endif

<form role="form" method="POST" action="{{ url('companies/new') }}">

	{{ csrf_field() }}

	<span class="form-heading">
		Company Details
	</span>

	<div class="row">
		<div class="col-md-12">
    		<input id="name" type="text" name="name" placeholder="Company Name" value="{{ old('name') }}" {{ $errors->has('name') ? 'class=has-error' : '' }} autofocus>
    	</div>
    </div>

	<div class="row">
		<div class="col-md-12">
			<select name="type" {{ $errors->has('type') ? 'class=has-error' : '' }}>
				<option value="">Please select your company type</option>
				@foreach(config('app.companyTypes') as $companyType)
				<option {{ (old('type') == $companyType['id'] ? 'selected':'') }} value="{{ $companyType['id'] }}">{{ $companyType['name'] }}</option>
				@endforeach
			</select>
    	</div>
    </div>

	<div class="row registration_number hidden">
		<div class="col-md-12">
    		<input {{ $errors->has('registration_number') ? 'class=has-error' : '' }} type="text" name="registration_number" placeholder="Company Registration Number" value="{{ old('registration_number') }}">
    	</div>
    </div>

    <hr>

	<span class="form-heading">
		Company Address
	</span>

	<div class="row">
		<div class="col-md-12">
    		<input {{ $errors->has('address_line_one') ? 'class=has-error' : '' }} type="text" name="address_line_one" placeholder="Address Line One" value="{{ old('address_line_one') }}">
    	</div>
		<div class="col-md-12">
    		<input type="text" name="address_line_two" placeholder="Address Line Two" value="{{ old('address_line_two') }}">
    	</div>
		<div class="col-md-12">
    		<input {{ $errors->has('city') ? 'class=has-error' : '' }} type="text" name="city" placeholder="Town / City" value="{{ old('city') }}">
    	</div>
		<div class="col-md-12">
    		<input {{ $errors->has('postcode') ? 'class=has-error' : '' }} type="text" name="postcode" placeholder="Postcode" value="{{ old('postcode') }}">
    	</div>
		<div class="col-md-12">
			<select {{ $errors->has('country_id') ? 'class=has-error' : '' }} name="country_id">
				<option value="">Please select your Country</option>
				@foreach(\RiderRentals\Country::orderBy('name')->get() as $country)
				<option value="{{ $country->id }}" {{ (old('country') == $country->id ? 'selected':'') }} >
					{{ $country->name }}
				</option>
				@endforeach
			</select>
    	</div>
	</div>

    <hr>

	<span class="form-heading">
		VAT registered?
	</span>

    <div class="row">
        <div class="col-md-12">

            <label class="radio-inline">
                <input name="vat_registered" type="radio" value="yes" {{ (old('vat_registered') == 'yes' ? 'checked' : '') }}> Yes
            </label>

            <label class="radio-inline">
                <input name="vat_registered" type="radio" value="no" {{ ((old('vat_registered') == 'no' || empty(old('vat_registered'))) ? 'checked' : '') }}> No
            </label>
		</div>
	</div>

	<div class="row vat-number hidden">
		<div class="col-md-12">
			<br>
    		<input {{ $errors->has('vat_number') ? 'class=has-error' : '' }} value="{{ old('vat_number') }}" id="vatNumber" type="text" name="vat_number" placeholder="VAT Number">
    	</div>
    </div>

	<hr>

	<div class="row">
		<div class="col-md-6 col-md-offset-6 text-right">
		    <button type="submit" class="button button-large button-primary">
		        Create Company
		    </button>
    	</div>
	</div>
</form>
@endsection