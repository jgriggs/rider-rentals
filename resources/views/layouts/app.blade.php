<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="{{ $viewName }}">
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="inner">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        RR
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    {!! \RiderRentals\Helpers\getMenu() !!}
                </div>
            </div>
        </nav>
        <div class="body-content">
            <div class="inner">
                @yield('content')
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script>
    baseURL = "{{ url('/') }}/api/";
    </script>
    <script src="/js/app.js"></script>
</body>
</html>
