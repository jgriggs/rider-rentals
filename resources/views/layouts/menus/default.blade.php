@extends('layouts.menus.base')

@section('right')
<li>
    <a href="{{ url('login') }}">Login</a>
</li>
<li>
    <a href="{{ url('register') }}">Register</a>
</li>
@endsection