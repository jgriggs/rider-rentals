@extends('layouts.menus.base')

@section('left')
@if (session('currentCompany'))
<li>
	<a href="{{ url('companies/manage') }}">Dashboard</a>
</li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Locations <span class="caret"></span></a>
	<ul class="dropdown-menu">
		{!! \RiderRentals\Helpers\getLocations() !!}
		<li>
			<a href="{{ url('companies/locations/new') }}"><span class="glyphicon glyphicon-plus"></span> New Location</a>
		</li>
	</ul>
</li>
<li>
	<a href="{{ url('companies/bookings') }}">Bookings</a>
</li>
@endif
@endsection

@section('right')
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ substr(\RiderRentals\Helpers\logoutName(), 0, 9) . '...' }} <span class="caret"></span></a>
	<ul class="dropdown-menu">
		@if (\RiderRentals\Helpers\logoutName() !== Auth::user()->name)
		<li>
			<a href="{{ url('/') }}"><span class="glyphicon glyphicon-user"></span> {{ Auth::user()->name }}</a>
		</li>
		<li class="divider"></li>
		@endif
		@if (count($companies = Auth::user()->companies()->where('name', '<>', \RiderRentals\Helpers\logoutName())->get()) > 0)
			<li class="helper">
				<span>Switch To</span>
			</li>
			@foreach ($companies as $company)
				<li>
					<a href="{{ url("companies/{$company->id}/manage") }}">{{ $company->name }}</a>
				</li>
			@endforeach
			<li class="divider"></li>
		@endif
		<li><a href="{{ url('companies/new') }}"><span class="glyphicon glyphicon-plus"></span> New Company</a></li>
		<li><a href="{{ url('logout') }}">Logout</a></li>
	</ul>
</li>
@endsection