<ul class="nav navbar-nav navbar-left">
	@yield('left')
</ul>
<ul class="nav navbar-nav navbar-right">
	@yield('right')
</ul>