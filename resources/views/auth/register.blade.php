@extends('layouts.app')

@section('content')

<h1>Register for Rider Rentals</h1>

<form id="registration-form" role="form" method="POST" action="{{ url('register') }}">

    {{ csrf_field() }}

	<div class="row">
		<div class="col-md-12">
    		<input id="name" type="text" name="name" placeholder="Name" value="{{ old('name') }}" {{ $errors->has('name') ? 'class=has-error' : '' }} required autofocus>
            @if ($errors->has('name'))
                <span class="help-block">
                    {{ $errors->first('name') }}
                </span>
            @endif
    	</div>
    </div>

    <div class="row">
    	<div class="col-md-12">
    		<input id="email" type="email" name="email" placeholder="Email Address" value="{{ old('email') }}" {{ $errors->has('email') ? 'class=has-error' : '' }} required>
            @if ($errors->has('email'))
                <span class="help-block">
                    {{ $errors->first('email') }}
                </span>
            @endif
    	</div>
    </div>

    <div class="row">
    	<div class="col-md-6">
    		<input id="password" type="password" name="password" placeholder="Password" {{ $errors->has('password') ? 'class=has-error' : '' }} required>
            @if ($errors->has('password'))
                <span class="help-block">
                    {{ $errors->first('password') }}
                </span>
            @endif
    	</div>
    	<div class="col-md-6">
    		<input id="password-confirm" type="password" name="password_confirmation" placeholder="Confirm Password" {{ $errors->has('password_confirmation') ? 'class=has-error' : '' }} required>
    	</div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-9">
            <span>Account Type</span>
            @foreach(\RiderRentals\Role::publicRoles() as $role)

            <label class="radio-inline">
                <input type="radio" name="account_type" @if (old('account_type') == $role->id || (empty(old('account_type')) && $loop->first)) checked="checked" @endif value="{{ $role->id }}"> {{ $role->name }}
            </label>
            @endforeach

            @if ($errors->has('account_type'))
                <span class="help-block">
                    {{ $errors->first('account_type') }}
                </span>
            @endif
        </div>
        <div class="col-md-3">
            <button type="submit" class="button button-large button-primary">
                Register
            </button>
        </div>
    </div>
</form>

@endsection
