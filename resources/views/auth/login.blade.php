@extends('layouts.app')

@section('content')
    <form role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}

        <div class="row">
            <div class="col-md-12">
                <input id="email" type="email" name="email" value="{{ old('email') }}" placeholder="Email Address" {{ $errors->has('email') ? 'class=has-error' : '' }} required autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input id="password" type="password" name="password" placeholder="Password" {{ $errors->has('password') ? 'class=has-error' : '' }} required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-4 text-right">
                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                    Forgot Your Password?
                </a>
                <button type="submit" class="button button-large button-primary">
                    Login
                </button>

            </div>
        </div>
    </form>
@endsection
