<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Pass a body class to all views
View::composer('*', function($view) {
	$view = explode('.', $view->getName());
	View::share('viewName', implode(' ', $view));
});

// Auth routes
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

// Admin / Tour Host routes
Route::group(['middleware' => ['auth', 'check-role:admin,tour-host']], function()
{
	Route::get('companies/new', 'CompaniesController@new');
	Route::post('companies/new', 'CompaniesController@create');
});

// Logged in routes (also redirect company owners to register their company if they don't have one)
Route::group(['middleware' => ['auth', 'registered-company']], function()
{
	Route::get('/', 'DashboardController@index');
	Route::get('home', 'DashboardController@index');

	Route::group(['prefix' => 'companies', 'middleware' => ['check-role:tour-host']], function()
	{
		Route::get('manage', 'CompaniesController@dashboard');
		Route::get('{id}/manage', 'CompaniesController@dashboard');


		Route::get('locations/manage', 'LocationsController@manage');
		Route::get('locations/new', 'LocationsController@new');
		Route::post('locations/new', 'LocationsController@create');
		Route::get('locations/{id}', 'LocationsController@manage');

		Route::get('bookings', 'BookingsController@view');

		// AJAX routes
		Route::get('companies/location/default', 'CompaniesController@defaultLocation');
	});
});

// Test Routes
Route::get('companies/{id}/delete', 'CompaniesController@delete');
