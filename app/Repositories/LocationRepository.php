<?php

namespace RiderRentals\Repositories;

use RiderRentals\Location;

class LocationRepository {

	public function new($data)
	{
		return Location::create($data);
	}

	public function getLocationsByCompany($companyId)
	{
		return Location::where('company_id', $companyId)->get();
	}

	public function get($id)
	{
		return Location::find($id);
	}
}