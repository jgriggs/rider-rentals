<?php

namespace RiderRentals\Repositories;

use RiderRentals\Company;

class CompanyRepository {

	public function new($data)
	{
		return Company::create($data);
	}

	public function get($id, $args = null)
	{
		$company = Company::where('id', $id);

		if (isset($args['select'])) {
			$company->select($args['select']);
		}

		$company = $company->first();
		return (!empty($company) ? $company : null);
	}

	public function update($args)
	{
		if (!isset($args['id']) || !isset($args['values']))
			return;

		Company::where('id', $args['id'])
				->update($args['values']);
	}

	public function ownsCompany($companyId)
	{
		foreach (\Auth::user()->companies as $company) {
			if ($company->id === (int)$companyId) {
				return true;
			}
		}
		return false;
	}


	public function setCurrentCompany($id)
	{
		$current = session('currentCompany');
		if (!isset($current['id']) || $current['id'] !== (int) $id) {
			$company = $this->get($id, [
				'select' => ['id', 'name']
			]);
			session(['currentCompany' => ['id' => $company->id, 'name' => $company->name]]);
		}
	}

}