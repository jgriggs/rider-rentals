<?php

namespace RiderRentals;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function company()
    {
    	return $this->belongsTo('RiderRentals\Company');
    }
}
