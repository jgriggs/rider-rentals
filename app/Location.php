<?php

namespace RiderRentals;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    protected $fillable = [
       'name', 'address_line_one', 'address_line_two', 'address_line_three', 'city', 'postcode', 'country_id', 'company_id',
    ];

    public function country() {
    	return $this->belongsTo('RiderRentals\Country');
    }

    public function company() {
    	return $this->belongsTo('RiderRentals\Company');
    }
}
