<?php

namespace RiderRentals;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function companies() {
    	return $this->hasMany('RiderRentals\Company');
    }

    public function locations() {
    	return $this->hasMany('RiderRentals\Location');
    }

    public function getShortNameAttribute($value) {
    	return strtoupper($value);
    }
}
