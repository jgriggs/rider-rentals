<?php

namespace RiderRentals\Providers;

use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \RiderRentals\Services\Payments\PaymentContract::class,
            \RiderRentals\Services\Payments\StripePayments::class
        );
    }
}
