<?php

namespace RiderRentals\Http\Middleware;

use Closure;

class RegisteredCompany
{
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->user()->hasRole('tour-host') && count($request->user()->companies) === 0) {
        	return redirect('companies/new');
        }

        return $next($request);
    }
}
