<?php

namespace RiderRentals\Http\Controllers;

use Illuminate\Http\Request;
use RiderRentals\Repositories\CompanyRepository;
use RiderRentals\Repositories\LocationRepository;

class LocationsController extends Controller
{

	protected $locations;

	public function __construct(LocationRepository $locations)
	{
		$this->locations = $locations;
	}

	public function manage($id = null)
	{
		$location = $this->locations->get($id);
	}

	public function new(Request $request, CompanyRepository $companies, $companyId = null)
	{
		if (!isset($companyId))
			$companyId = \RiderRentals\Helpers\getACompanyId($request);

		if (!$companies->ownsCompany($companyId))
			return redirect('/');
        $companies->setCurrentCompany($companyId);

		return view('locations.new')->with(['companyId' => $companyId]);
	}

	public function create(Request $request, CompanyRepository $companies)
	{
		if (!$companies->ownsCompany($request->input('company_id')))
			return redirect('/');

        // Validate
        $this->validate($request, [
        	'name' => 'required',
            'address_line_one' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'country_id' => 'required',
        ]);

        $location = $this->locations->new($request->except(['_token']));

        return redirect('companies/locations/'.$location->id);
	}
}
