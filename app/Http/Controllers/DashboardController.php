<?php

namespace RiderRentals\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function index(Request $request)
    {
    	\RiderRentals\Helpers\clearCurrentCompany();
    	return view('home');
    }
}
