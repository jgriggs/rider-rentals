<?php

namespace RiderRentals\Http\Controllers;

use Illuminate\Http\Request;
use RiderRentals\Repositories\CompanyRepository;
use RiderRentals\Services\Payments\PaymentContract;

class CompaniesController extends Controller
{

    private $companies;

    public function __construct(CompanyRepository $companies)
    {
    	$this->companies = $companies;
    }

    // View methods
    public function dashboard(Request $request, PaymentContract $payments, $companyId = null)
    {
        // If the ID isn't set, try loading from session
        if (!isset($companyId))
            $companyId = \RiderRentals\Helpers\getACompanyId($request);

        if (!$this->companies->ownsCompany($companyId))
            return redirect('/');

        $this->companies->setCurrentCompany($companyId);

        $company = $this->companies->get($companyId);
        $stripeAccount = $payments->retrieveCompany($company->stripe_id);
        return view('companies.manage');
    }

    public function new()
    {
        \RiderRentals\Helpers\clearCurrentCompany();
    	return view('companies.new');
    }

    // Database methods
    public function create(Request $request, PaymentContract $payments)
    {
        // Validate
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'registration_number' => 'required_if:type,ltd,plc',
            'address_line_one' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'country_id' => 'required',
            'vat_number' => 'required_if:vat_registered,yes',
        ]);

        // Create the company
        $company = $this->companies->new($request->except(['_token', 'vat_registered']));

        // Add them to Stripe
        $stripeResponse = $payments->createCompany([
            'business_name' => $company->name,
            'country' => $company->country->short_name,
            'email' => $request->user()->email,
            'managed' => true,
            'tos_acceptance' => [
                'date' => time(),
                'ip' => $request->ip(),
            ],
        ]);

        // Add their Stripe ID to the database
        $this->companies->update([
            'id' => $company->id,
            'values' => [
                'stripe_id' => $stripeResponse->id,
            ],
        ]);

        // Create a relationship between user and company
        \DB::table('users_companies')->insert([
            'user_id' => $request->user()->id,
            'company_id' => $company->id,
        ]);

        // Redirect to the company dashboard
        return redirect("companies/{$company->id}/manage/");
    }

    public function delete(PaymentContract $payments, $id)
    {
        // Move this to the repository
        $company = $this->companies->get($id);

        // Delete all relationships
        \DB::table('users_companies')->where('id', $id)->delete();

        // Delete the Stripe account
        if (isset($company->stripe_id))
            $payments->deleteCompany($company->stripe_id);

        // Delete the record
        $company->delete();

        return redirect('/');
    }
}
