<?php

namespace RiderRentals\Http\Controllers;

use Illuminate\Http\Request;
use RiderRentals\Services\Payments\PaymentContract;

class PaymentsController extends Controller
{
	protected $payments;

	public function __construct(PaymentContract $payments)
	{
		$this->payments = $payments;
	}

	public function charge()
	{
	}
}
