<?php

namespace RiderRentals;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	public $timestamps = false;

    public function user() {
        return $this->hasOne('RenterRiders\User');
    }

    public static function publicRoles() {
    	return self::where('public', 1)->get();
    }

}
