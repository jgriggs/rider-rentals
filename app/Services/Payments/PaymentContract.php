<?php

namespace RiderRentals\Services\Payments;

interface PaymentContract {
	public function charge($args);
	public function createCompany($args);
	public function deleteCompany($id);
	public function retrieveCompany($id);
}
