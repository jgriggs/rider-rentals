<?php

namespace RiderRentals\Services\Payments;

use Stripe\Stripe;
use Stripe\Account;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class StripePayments implements PaymentContract {

	public function __construct() {
		Stripe::setApiKey(env('STRIPE_SECRET'));
	}

	public function charge($args) {
	}

	public function createCompany($args) {
		return Account::create($args);
	}

	public function retrieveCompany($id) {
		// Check the cache first
		if (Cache::has('company_'.$id.'_stripe')) {
			return Cache::get('company_'.$id.'_stripe');
		}
		$account = Account::retrieve($id);
		Cache::put('company_'.$id.'_stripe', $account, Carbon::now()->addDays(1));
		return $account;
	}

	public function deleteCompany($id) {
		$account = $this->retrieveCompany($id);
		$account->delete();
	}

}