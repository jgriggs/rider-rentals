<?php

namespace RiderRentals;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name', 'registration_number', 'vat_number', 'type', 'address_line_one', 'address_line_two', 'address_line_three', 'city', 'country_id'
    ];

    public function users() {
        return $this->belongsToMany('RiderRentals\User', 'users_companies');
    }

    public function locations() {
        return $this->hasMany('RiderRentals\Location');
    }

    public function items() {
        return $this->hasMany('RiderRentals\Item');
    }

    public function country() {
    	return $this->belongsTo('RiderRentals\Country');
    }
}
