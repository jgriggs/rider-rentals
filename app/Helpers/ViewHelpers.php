<?php

namespace RiderRentals\Helpers;

use Illuminate\Support\Facades\Auth;

function getMenu() {
	$menu = (Auth::check() ? Auth::user()->getRole() : 'default');
	return view("layouts/menus/$menu");
}

function logoutName() {
	$current = session('currentCompany');
	if (empty($current))
		return Auth::user()->name;
	return $current['name'];
}

function clearCurrentCompany()
{
	if (Auth::user()->hasRole('tour-host') && !empty(session('currentCompany')))
		session()->forget('currentCompany');
}