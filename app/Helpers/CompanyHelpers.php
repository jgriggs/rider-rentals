<?php

namespace RiderRentals\Helpers;

function getACompanyId($request = null) {
    $companyId = session('currentCompany');
    if (isset($companyId['id'])) {
        $companyId = $companyId['id'];
    } else {
    	$request = resolve('Illuminate\Http\Request');
        // If the session isn't set, get the first company from the user
        $companyId = $request->user()->companies()->first()->id;
    }

    return $companyId;
}

function getLocations() {
	$companyId = getACompanyId();
	if (!empty($companyId)) {
		$locations = resolve('RiderRentals\Repositories\LocationRepository');
		$locations = $locations->getLocationsByCompany($companyId);
		if (count($locations) > 0) {
			foreach ($locations as $location) {
				$url = url("companies/locations/{$location->id}");
				echo "<li><a href='$url'>{$location->city}</a></li>";
			}
		}
	}
}