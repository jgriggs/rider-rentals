<?php

namespace RiderRentals;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('RiderRentals\Role');
    }

    public function companies()
    {
        return $this->belongsToMany('RiderRentals\Company', 'users_companies');
    }

    public function getRole()
    {
        return strtolower(str_replace(' ', '-', $this->role->name));
    }

    // Check a user has a role
    public function hasRole($roleName)
    {
        return ($roleName === strtolower(str_replace(' ', '-', $this->role->name)));
    }
}
